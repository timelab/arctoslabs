<?php

/**
 * Featured Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['section_title'] = get_field('acf_fc_title');
$context['boxes'] = get_field('acf_fc_feature_boxes');

Timber::render('templates/blocks/featured-section.twig',  $context);
?>