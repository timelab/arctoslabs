<?php

/**
 * Text Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['ts_text'] = get_field('acf_ts_text');

$width = get_field('acf_ts_width');
$context['ts_col_style'] = ( $width === 'wide') ? 'col-md-10 col-lg-8' : 'col-md-6';

Timber::render('templates/blocks/text-section.twig',  $context);
?>