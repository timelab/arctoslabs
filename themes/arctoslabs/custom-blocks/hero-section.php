<?php

/**
 * Hero Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['hs_image'] = get_field('acf_hs_background_image');
$context['text_alignment'] = get_field('acf_hs_text_alignment');
$context['hs_small_text_above'] = get_field('acf_hs_small_text');
$context['hs_large_text'] = get_field('acf_hs_large_text');
$context['hs_small_text_below'] = get_field('acf_hs_small_text_below');
$context['hs_button'] = get_field('acf_hs_button');
$context['row_class'] = "";

switch($context['text_alignment']){
  case "text-center":
    $context['row_class'] .= "justify-content-center";
    break;
  case "text-right":
    $context["row_class"] = "justify-content-end";
    break;
}

Timber::render('templates/blocks/hero-section.twig',  $context);
?>