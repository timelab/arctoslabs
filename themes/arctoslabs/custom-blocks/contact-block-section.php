<?php

/**
 * Contact Block Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['email'] = get_field('acf_cobs_email');
$context['phone'] = get_field('acf_cobs_phone');
$context['text'] = get_field('acf_cobs_text');
$context['form'] = get_field('acf_cobs_form_shortcode');
$context['form'] = str_replace(']', ' html_class="form-email"]', $context['form']);
$context['map_api_key'] = get_field('acf_google_maps_api_key', 'option');
$context['address'] = get_field('acf_footer_address_group', 'option');


Timber::render('templates/blocks/contact-block-section.twig',  $context);
?>