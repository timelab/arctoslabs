<?php

/**
 * Career Block Section Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$context['block'] = $block;
$context['blocks'] = get_field('acf_cbs_career_block');

Timber::render('templates/blocks/career-blocks-section.twig',  $context);
?>